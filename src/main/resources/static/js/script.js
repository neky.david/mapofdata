/**EVENTS**/

var mainInfoHeight = -1;

/*RESIZE WINDOW*/
window.onresize = function (event) {
    //posunutí mainSearch
    var info = $(".info.mainInfo");
    if (info && mainInfoHeight !== info.offsetHeight) {
        mainInfoHeight = info.offsetHeight;
        positionUnderInfo();
    }
};

/*CLICK WINDOW*/
window.onclick = function (event) {
    //ativování/deaktivování mainSearch
    var mainSearch = $('#mainSearch');
    if (mainSearch) {
        if ($(event.target).closest("#mainSearch").length && !$(event.target).closest("#mainSearch .indicator").length) {
            mainSearch.addClass("active");
            mainSearch.find('input').focus();
        } else {
            mainSearch.removeClass("active");
        }
    }
};

/*MOUSE MOVE*/
$(document).bind('mousemove', function(e){
    $('#cursorSpy').css({
        left:  e.pageX + 20,
        top:   e.pageY
    });
});

/** SIDENAV **/
var sidenav = document.getElementById("topicSidenav");
var sidenavWidth = "310px";//"330px";

function moveNav() {
    if (sidenav.offsetWidth > 0) { //CLOSE
        hideSecondLevel();
        $("#topicSidenav").css("width", "0");
        $("#openSidenavBtn svg").removeClass("mirrorX");
    } else { //OPEN
        $("#topicSidenav").css("width", sidenavWidth);
        $("#openSidenavBtn svg").addClass("mirrorX");
        positionUnderInfo();
    }
}

function topicClick(event) {
    if (event && event.target) {
        var topic = event.target.closest(".topic");
        var topicIsActive = topic.classList.contains("active");
        var id = findData(event.target, "topicId");
        //deactive all topics
        $(".topic").removeClass("active");

        if (topicIsActive && secondLevelIsShow()) {
            //close indicators if click on active topic
            hideSecondLevel();
        } else {
            //active topic
            if (topic) {
                $(topic).addClass("active");
            }
            bleachNonActiveTopics();

            showIndicatorList(id);
            showSecondLevel();
        }
        positionUnderInfo();
    }
}

function topicMouseOver(event) {
    if (event && event.target) {
        bleachNonActiveTopics();

        var color = findData(event.target, "color");
        if (color) {
            var topic = event.target.closest(".topic");
            if (topic) {
                var svg = $(topic).find("svg");
                if (svg) {
                    $(svg).css("color", color);
                }
            }
        }
    }
}

function bleachNonActiveTopics() {
    $(".topic").not(".active").find(".topicIcon svg").css("color", "inherit");
}

function topicMouseOut(event) {
    if (event && event.target) {
        var topic = event.target.closest(".topic");
        if (!topic || !$(topic).hasClass("active")) {
            var svg = $(event.target).find("svg");
            if (svg) {
                $(svg).css("color", "inherit");
            }
        }
    }
}

function showSecondLevel() {
    $("#topicSidenav").css("width", "600px");
    $(".secondLevel").css("display", "block").addClass("open"); //.css("display", "block").css("width", "50%");
}

function hideSecondLevel() {
    $("#topicSidenav").css("width", sidenavWidth);
    $(".secondLevel").removeClass("open"); //.css("width", "0");
    setTimeout(function () {
        $(".secondLevel").css("display", "none");
    }, 300);
}

function secondLevelIsShow() {
    var secondlevel = $(".secondLevel");
    return secondlevel.hasClass("open");
    /*var width = $(".secondLevel").css("width");
    return width === "600px";*/
}

function showIndicatorList(id) {
    $(".indicatorList").addClass("d-none");
    $("#indicators" + id).removeClass("d-none");
}

/**UNDER INFO**/
function positionUnderInfo() {
    /*var info = $(".info.mainInfo");
    var div = $("#underInfoDiv");

    if (info && div) {
        div.css("top", getCompleteHeight(info[0]));
    }*/
}

/**SEARCH**/

function mainSearch(event) {
    var val = event.target.value;
    if (val.length > 1) {
        var resultDiv = document.querySelector('#mainSearch .searchResult'); //$('#mainSearch .searchResult');

        if (!resultDiv) {
            console.error("MapOfData ERROR: Nebyl nalezen element .searchResult!")
        } else {
            $.ajax({
                url: '/ajax/findIndicatorsByName',
                type: 'POST',
                data: {value: val}, //JSON.stringify(orderId)
                success: function (data) {

                    resultDiv.innerHTML = '';

                    for (i = 0; i < data.length; i++) {
                        var indicator = data[i];
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        b.classList.add("indicator");
                        b.innerHTML = "" +
                            "<div class=\"d-flex\">" +
                            "<p>" + indicator.name + "</p>" +
                            "</div>";
                        b.dataset.indicatorId = indicator.id;

                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function (e) {
                            $('#mainSearch').removeClass("active");
                            indicatorClick(e);
                        });
                        resultDiv.appendChild(b);
                    }
                },
                error: function (a, b, c) {
                    console.error(a, b, c);
                }
            });
        }
    }else{
        removeMainSearchResult();
    }
}

function mainSearchButtonClick(event){
    var search = $(event.target).closest('#mainSearch');
    if(search && search.hasClass("active")){
        search.find("input").val("");
        removeMainSearchResult();
    }
}

function removeMainSearchResult() {
    $("#mainSearch .searchResult").empty();
}

/**SPINNER**/
var spinner = $('#spinner');

function detectSpinner() {
    if (!spinner) {
        console.error("Nelze volat metody showSpinner nebo hideSpinner pokud není element spinner na stránce!");
        return false;
    }
    return true;
}

function showSpinner() {
    if (detectSpinner()) {
        spinner.css('display', 'block');
    }
}

function hideSpinner() {
    if (detectSpinner()) {
        spinner.css('display', 'none');
    }
}

function showElementById(selector) {
    $(selector).css('display', 'block');
}

function hideElementById(selector) {
    $(selector).css('display', 'none');
}

/*TEST*/
function testSpinner() {
    setTimeout(function () {

        showSpinner();

        setTimeout(function () {
            hideSpinner();
        }, 5000);

    }, 5000);
}
