var maxValue, minValue, median;
var indicatorData, differentData = [];
var inicatorName = "";
//rozdělení do kategorií
var grades = [];
var colorPalet = generatColorPalet();
var info, legend;
var map;
var geoIp;
var isGeoCalled = false;
var lastClickedLayer;
var valuesCount;

initMap();
loadData(null);

function indicatorClick(event) {
    var indicId = findData(event.target, "indicatorId");
    if (indicId) {
        $.cookie("selectIndicator", indicId);
        //moveNav();
        showSpinner();
        geojson.remove();
        loadData(indicId);
    } else {
        log.error("Connot find indicator key in element");
        //TODO error
    }
}

/**LOAD DATA**/
function loadData(indicatorId) {
    if (!info) {
        //TODO show error
        console.error("Info element is not initialize!");
        return;
    }

    if (!indicatorId) {
        indicatorId = $.cookie("selectIndicator") ? $.cookie("selectIndicator") : "SP.POP.TOTL";
    }

    $.get("http://api.worldbank.org/v2/country/all/indicator/" + indicatorId + "?per_page=1000&mrv=1&format=json",
        function (data, status) {
            if (status === "success" && data.length > 1) {
                indicatorData = data[1];

                if (indicatorData) {
                    inicatorName = indicatorData[0].indicator.value;

                    if (!isGeoCalled) {
                        findGeoIp();
                    } else {
                        //zobrazení INFO
                        info.update(null, true);
                    }
                    //seřazení dat
                    processData();
                    //generování kategorií
                    generateGrades();
                    //zobrazení legendy
                    legend.update();
                    //zobrazit BAREVNOU VRSTVU
                    addDataToMap();
                } else {
                    console.error("No Data was found!")
                    //TODO SHOW and LOG ERROR
                }
            } else {
                console.error("Data load failed!")
                //TODO SHOW and LOG ERROR
            }

            hideSpinner();
        });
}

/**INIT**/
function initMap() {
    map = L.map('map', {
        zoomControl: false,
        worldCopyJump: true,
        minZoom: 2,
        maxBounds: [
            [-90, -Infinity],
            [90, Infinity]
        ]
    });
    map.setView([45, 20], 3);

    //ZOOM
    L.control.zoom({
        position: 'bottomright'
    }).addTo(map);

    addLayers();
    addInfo();
    addUnderInfoDiv();
    addLegend();
}

/**LAYERS**/
function addLayers() {
    map.createPane('labels');

    // https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}{r}.png
    // https://{s}.basemaps.cartocdn.com/rastertiles/voyager_nolabels/{z}/{x}/{y}{r}.png
    // https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png
    L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager_nolabels/{z}/{x}/{y}{r}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
        subdomains: 'abcd',
        maxZoom: 19
    }).addTo(map);

    /**LABEL MAP**/
    // https://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}{r}.png
    // https://{s}.basemaps.cartocdn.com/rastertiles/voyager_only_labels/{z}/{x}/{y}{r}.png
    // https://stamen-tiles-{s}.a.ssl.fastly.net/terrain-labels/{z}/{x}/{y}{r}.{ext}
    L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager_only_labels/{z}/{x}/{y}{r}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
        subdomains: 'abcd',
        maxZoom: 19,
        pane: 'labels'
    }).addTo(map);
}

/**INFO**/
function addInfo() {
    info = L.control({position: 'topleft'});

    // method that we will use to update the control based on feature properties passed
    info.update = function (props, doChange) {
        if (doChange) {
            var data, unit;
            var state = "-";
            var value = "-";
            var decimalPlus = 2;

            if (props) {
                data = getStateData(props.ISO3);
                state = props.NAME; //data.country.value

                if (data) {
                    value = formatNumber(data.value, decimalPlus);
                    unit = data.unit;
                }
            }
            //žádná data nenalezena - cursor neukazuje na žádnou zemi
            else {
                //existuje informace o geolokaci
                if (geoIp) {
                    data = getStateDataByCountryId(geoIp.countryCode);
                    if (data) {
                        state = data.country.value;
                        value = formatNumber(data.value, decimalPlus);
                        unit = data.unit;
                    }
                }
            }

            document.getElementById('cursorSpy').innerHTML =
                '<p class="state">' + state + '</p>' +
                '<p><span class="value">' + value + '</span>' +
                ((unit) ? '<span class="unit">' + unit + '</span></p>' : '');

            this._div.getElementsByClassName('infoTitle')[0].innerText = inicatorName;
        }
    };

    info.onAdd = function (map) {
        var div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
        div.classList.add("mainInfo");
        div.innerHTML = "<span class='infoTitle'></span><div class='staticPart'></div>";
        this._div = div;
        this.update();
        return this._div;
    };

    info.addTo(map);
}

function addUnderInfoDiv() {
    var infoDiv = $('.info.mainInfo.leaflet-control .staticPart');
    if (!infoDiv.length) {
        console.error("addUnderInfoDiv: Nebyl nalezen element mainInfo!");
    } else {
        var underInfoDiv = L.DomUtil.create('div');
        underInfoDiv.id = 'underInfoDiv';
        var underInfoDivContent = $('#underInfoDivContent').detach();
        underInfoDiv.innerHTML = underInfoDivContent[0].innerHTML;
        infoDiv[0].append(underInfoDiv);
    }
}

/**LEGEND**/
function addLegend() {
    legend = L.control({position: 'bottomleft'});

    legend.update = function () {
        var gColorIndex = grades.length - 2;

        this._div.innerHTML = "";
        for (var i = 0; i < grades.length; i++) {
            this._div.innerHTML +=
                '<p><i style="background:' + colorPalet[gradesColorIndexes[gColorIndex][i]] + '"></i>' +
                '<=&nbsp;' + formatNumber(grades[i], 2) + '</p>'
        }
    };

    legend.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info legend d-none d-sm-block');
        this.update();
        return this._div;
    };

    legend.addTo(map);
}

/**ACTIONS**/
function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: resetAndHIghlight
    });
}

//MOUSE OVER
function highlightFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }

    info.update(layer.feature.properties, true);

    //show spy
    $('#cursorSpy').removeClass('d-none');
}

//MOUSE OUT
function resetHighlight(e) {
    geojson.resetStyle(e.target);

    //hide spy
    $('#cursorSpy').addClass('d-none');

    info.update();
}

//CLICK
function resetAndHIghlight(e) {
    //TODO reset
    /*if (lastClickedLayer)
        geojson.resetStyle(lastClickedLayer);*/
    geojson.resetStyle();

    lastClickedLayer = e.target;
    highlightFeature(e);
}


/**DATA**/
function addDataToMap() {
    geojson = L.geoJson(bordersData, {
        style: dataLayerStyle,
        onEachFeature: onEachFeature
    }).addTo(map);
}

//GRADES
function generateGrades() {
    var lenght = differentData.length; //indicatorData.length;
    grades = [];
    if (differentData && lenght) {

        var gradeIndex = findGradeIndex(lenght);

        if (gradeIndex > -1) {
            lenght--; //převod velikosti na index

            grades[0] = differentData[Math.round(lenght * gradesDist[gradeIndex][0] / 100)];  //3
            if (gradeIndex > 0) {
                grades[1] = differentData[Math.round(lenght * gradesDist[gradeIndex][1] / 100)]; //7
                if (gradeIndex > 1) {
                    grades[2] = differentData[Math.round(lenght * gradesDist[gradeIndex][2] / 100)]; //11
                    if (gradeIndex > 2) {
                        grades[3] = differentData[Math.round(lenght * gradesDist[gradeIndex][3] / 100)]; //14
                        if (gradeIndex > 3) {
                            grades[4] = differentData[Math.round(lenght * gradesDist[gradeIndex][4] / 100)]; //15
                            if (gradeIndex > 4) {
                                grades[5] = differentData[Math.round(lenght * gradesDist[gradeIndex][5] / 100)]; //15
                                if (gradeIndex > 5) {
                                    grades[6] = differentData[Math.round(lenght * gradesDist[gradeIndex][6] / 100)]; //14
                                    if (gradeIndex > 6) {
                                        grades[7] = differentData[Math.round(lenght * gradesDist[gradeIndex][7] / 100)]; //11   //////90
                                        if (gradeIndex > 7) {
                                            grades[8] = differentData[Math.round(lenght * gradesDist[gradeIndex][8] / 100)]; //7    //////97
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            grades.push(differentData[Math.round(lenght * gradesDist[gradeIndex][gradeIndex + 1] / 100)]);//3    //indicatorData[parseInt(lenght * 100 / 100)].value;

            console.log("GRADES: " + grades);

        } else {
            console.error("grade index is under zero!");
        }
    }
}

function findGradeIndex(lenght) {
    for (var i = 0; i < gradesValuesCount.length; i++) {
        if (lenght < gradesValuesCount[i]) {
            return i;
        }
    }
    return -1;
}

//vrací položku ze seznamu hodnot (podle countryiso3code)
function getStateData(stateId) {
    var value = null;
    if (indicatorData) {
        for (var i = 0; i < indicatorData.length; i++) {
            var data = indicatorData[i];
            if (data.countryiso3code === stateId) {
                value = data;
                i = indicatorData.length;
            }
        }
    }
    return value;
}

//vrací položku ze seznamu hodnot (podle country.key)
function getStateDataByCountryId(countryId) {
    var value = null;
    if (indicatorData) {
        for (var i = 0; i < indicatorData.length; i++) {
            var data = indicatorData[i];
            if (data.country && data.country.id === countryId) {
                value = data;
                i = indicatorData.length;
            }
        }
    }
    return value;
}

//seřazení dat & odstranění nepotřebných
function processData() {
    if (indicatorData) {

        //REMOVE NON COUNTRY DATA and EMPTY DATA
        for (var i = 0; i < indicatorData.length; i++) {
            var data = indicatorData[i];

            if (noneCountryCodes.includes(data.countryiso3code)) {
                indicatorData.splice(i, 1);
                i--;
            } else if (!data.value) {
                indicatorData.splice(i, 1);
                i--;
            }
        }
        console.log("DATA COUNT: " + indicatorData.length);
        //SORT
        indicatorData.sort(sortCompare);
        //COUNT VALUES
        valuesCount = countDifferentValues(indicatorData);

        maxValue = indicatorData[indicatorData.length - 1].value;
        minValue = indicatorData[0].value;
        median = indicatorData[parseInt(indicatorData.length / 2)].value;
        console.log("maxValue: " + maxValue);
        console.log("minValue: " + minValue);
        console.log("median: " + median);
    } else {
        console.error("Can't sort values, cause data is empty!");
    }
}

function sortCompare(a, b) {
    if (a.value < b.value) {
        return -1;
    }
    if (a.value > b.value) {
        return 1;
    }
    return 0;
}

function countDifferentValues(data) {
    var prevVal = null;
    differentData = [];
    var count = 0;
    for (var i = 0; i < data.length; i++) {
        var indicVal = data[i].value;
        if (indicVal !== prevVal) {
            differentData.push(indicVal);
            count++;
        }
        prevVal = indicVal;
    }
    console.log("Number of Deffirent Values: " + count);
    return count;
}

/**STYLE**/
function generatColorPalet() {
    return redPalette; //brownPalette
}

function getColor(d) {
    if (grades && colorPalet && d) {

        var gColorIndex = grades.length - 2;

        for (var i = 0; i < grades.length; i++) {
            if (d <= grades[i]) {
                return colorPalet[gradesColorIndexes[gColorIndex][i]];//return colorPalet[gColorIndex[i]];
            }
        }
    }
    return "#fff"; //#fff
}

function dataLayerStyle(feature) {
    var data = getStateData(feature.properties.ISO3);
    var color = data ? getColor(data.value) : "#fff"; //#fff
    return {
        fillColor: color,//(feature.properties.density),
        weight: 2,
        opacity: data ? 1 : 0,
        color: 'white',
        dashArray: '3',
        fillOpacity: color === "#fff" ? 0 : 0.7 //#fff  0.65
    };
}

//http://api.worldbank.org/v2/topic?format=json
//http://api.worldbank.org/v2/indicator?format=json
//http://api.worldbank.org/v2/topic/19/indicator

//?mrv=2 - vrátí poslední dva roky, které obsahují nějakou hodnotu

/**GEOLOCATION**/
function findGeoIp() {
    geoIpAjax(function (response) {
        //console.log(response);
        geoIp = response;
        info.update(null, true);
    }, function (data, status) {
        console.error("geoIp failed!", status);
    })
}