
redPalette = ['#fffebb', '#fefba5', '#feda76', '#fdc564', '#fdb43c', '#fc9037', '#fc7530', '#e3542c', '#bd2421', '#8c001e' ];
brownPalette = ['#f1f9b0', '#efe386', '#e8d565', '#d7b653', '#cd9f4a', '#bb8748', '#a9743c', '#95553a', '#7e4230', '#512121' ];
grayPalette = ['#e2e2e2', '#cccccc', '#b9b9b9', '#a4a4a4', '#8c8c8c', '#7a7a7a', '#676767', '#444444', '#3a3a3a', '#2a2a2a' ];

viridisPalette = ['#fde725', '#b5de2c', '#6ece58', '#37b878', '#1f9f88', '#25838e', '#30678e', '#3e4989', '#482878', '#440d54' ];
magmaPalette = ['#fcfdbf', '#feca8d', '#fd9668', '#f2615d', '#ce4071', '#a02f7f', '#711f81', '#431476', '#190f3c', '#000003' ];
ggPlotPalette = ['#56b1f7', '#4ea1e1', '#4591cc', '#3e81b7', '#3772a3', '#2f638f', '#28547a', '#214667', '#1a3855', '#132b42' ];
brewerBluesPalette = ['#f5fafe', '#e1edf8', '#ccdff1', '#acd0e6', '#84bbdb', '#5ba2cf', '#3886c0', '#1e68af', '#074d96', '#08306b' ];
brewerYGBPalette = ['#fffed9', '#eff9b6', '#d0edb3', '#9bd7b9', '#63c1c0', '#36a7c2', '#227fb8', '#2554a3', '#23318d', '#081d58' ];
rainbowPalette = ['#ff3def', '#a238ff', '#0433ff', '#01abff', '#04fa9f', '#17f900', '#81fa00', '#f8fb00', '#ff8300', '#ff2500' ];
heatPalette = ['#fffff3', '#fffc8b', '#fffb1d', '#ffe400', '#ffbf00', '#ff9600', '#ff7100', '#ff4b00', '#ff2600', '#ff2500' ];


fffPalette = ['#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff' ];