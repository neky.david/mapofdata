package cz.david.mapofdata.shedule;

import com.fasterxml.jackson.databind.JsonNode;
import cz.david.mapofdata.entities.BaseIndicator;
import cz.david.mapofdata.entities.Indicator;
import cz.david.mapofdata.entities.Topic;
import cz.david.mapofdata.helpers.FileHelper;
import cz.david.mapofdata.helpers.JsonHelper;
import cz.david.mapofdata.helpers.RestHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class TopicsMaker {

    private static final Logger log = LoggerFactory.getLogger(TopicsMaker.class);
    private RestHelper restHelper;

    //second, minute, hour, day, month, weekday
    @Scheduled(cron = "0 0 2 * * *") //každý den ve 2 ráno
    //@Scheduled(fixedRate = 2000000) //1000 = 1s
    public void generateTopicsFiles() {
        log.info("Topic generator has been started!");

        try {

            restHelper = new RestHelper();
            JsonHelper jh = new JsonHelper();

            List<Topic> topics;

            /*int oldTopicCount = jh.getCountOfData(FileHelper.readTopicFile(), false);
            int newTopicCount = dataNode.size();*/

            //přibyl nový topic - musíme vygenerovat nový soubor
            //if (oldTopicCount < newTopicCount) {

                List<Topic> oldTopics = Topic.getListOfTopicsFromFile();
                List<Topic> newTopics = jh.getTopicsFromJSON(jh.getDataPartNode(restHelper.getTopicsJson()).toString());

                topics = Topic.uniteTopics(oldTopics, newTopics);

            /*} else { //použijeme starý
                topics = jh.getListFromJSON(new Topic(), FileHelper.readTopicFile(), Topic.class);
            }*/

            int indicNumber = 1;

            for (Topic topic :
                    topics) {
                //pro každý topic zjistíme aktuální indikátory
                List<Indicator> indicators = jh.getIndicatorsFromJSON(
                        jh.getDataPart(restHelper.getIndicatorsJson(topic.getId())));

                if (topic.getBaseIndicators() == null) topic.setBaseIndicators(new ArrayList<>());

                log.info("-------------------------");
                log.info(topic.getValue());

                for (Indicator indic :
                        indicators) {
                    log.info(indicNumber + ". " + indic.getKey() + " - " + indic.getName());
                    indicNumber++;

                    BaseIndicator baseIndicator = null;

                    //nalezení srpávnému BaseIndicator
                    for (int bi = 0; bi < topic.getBaseIndicators().size(); bi++) {
                        BaseIndicator base = topic.getBaseIndicators().get(bi);

                        if (indic.getBaseIndicatorKey().equals(base.getKey())) {
                            baseIndicator = base;
                            bi = topic.getBaseIndicators().size();
                        }
                    }

                    //nenašel se odpovídající baseIndicator - vytvoříme nový
                    if (baseIndicator == null) {
                        baseIndicator = new BaseIndicator();
                        baseIndicator.setKey(indic.getBaseIndicatorKey());
                        baseIndicator.setIndicators(new ArrayList<>());
                        topic.getBaseIndicators().add(baseIndicator);
                    }

                    baseIndicator.getIndicators().add(indic);
                }

                //nalezení společného názvu pro všechny indikátory
                for (BaseIndicator baseIndicator :
                        topic.getBaseIndicators()) {
                    baseIndicator.findNameFromIndicators();
                    log.info("BI: " + baseIndicator.getKey() + " - " + baseIndicator.getName());
                }
            }

            FileHelper.writeTopicFile(jh.serializObject(topics));
            log.info("File TOPICS.JSON was successfully created!");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
