package cz.david.mapofdata.controllers;

import cz.david.mapofdata.entities.Indicator;
import cz.david.mapofdata.entities.Topic;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;


@Controller
public class MapController {

    /**
     * MAP
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String map(Model model) {

        try {
            model.addAttribute("topics", Topic.getListOfTopicsFromFile());
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return "map";
    }

    @RequestMapping(value = "/ajax/findIndicatorsByName", method = RequestMethod.POST)
    public ResponseEntity<?> productAutocomplete(String value) {

        List<Indicator> indicators = new ArrayList<>();
        try {
            indicators = Indicator.findIndicatorsByName(value, 15);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return ResponseEntity.ok(indicators);
    }

}
