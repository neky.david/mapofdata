package cz.david.mapofdata.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

@Data
public class Indicator {

    @JsonProperty("id")
    private String key;
    @JsonProperty
    private String name;
    @JsonProperty
    private String unit;
    @JsonIgnore
    private String sourceNote;
    @JsonIgnore
    private String sourceOrganization;

    private String baseIndicatorKey;

    public static List<Indicator> findIndicatorsByName(String value, int maxCount) throws Exception {
        List<Indicator> indicators = new ArrayList<>();
        List<Topic> topics = Topic.getListOfTopicsFromFile();

        if (maxCount < 1)
            maxCount = 20;

        value = Normalizer.normalize(value, Normalizer.Form.NFD).toLowerCase();

        int valueBeginCount = 0;

        for (Topic topic :
                topics) {
            for (BaseIndicator baseIndic :
                    topic.getBaseIndicators()) {
                for (Indicator indic :
                        baseIndic.getIndicators()) {
                    //existuje v listu indikátor ze stejným klíčem - neprovádí se porovnání
                    if(indicators.stream().noneMatch(i -> i.getKey().equals(indic.getKey()))) {

                        String name = Normalizer.normalize(indic.getName(), Normalizer.Form.NFD).toLowerCase();

                        //název ZAČÍNÁ hledanou hodnotou - indikátor vložíme na začátek listu
                        if (name.startsWith(value)) {
                            indicators.add(0, indic);
                            valueBeginCount++;

                            //list je větší než maxCount - odstraníme poslední záznam
                            if (indicators.size() > maxCount) {
                                indicators.remove(indicators.size() - 1);
                            }
                        }
                        //název OBSAHUJE hledanou hodnotou - indikátor vložíme na konec listu
                        else if (indicators.size() < maxCount && indic.getName().contains(value)) {
                            indicators.add(indic);
                        }

                        //list obsahuje maximalní počet záznámů, které začínají hledanou hodnotou - return
                        if (valueBeginCount >= maxCount) {
                            return indicators;
                        }
                    }
                }
            }
        }

        return indicators;
    }

    public String getBaseIndicatorKey() {
        if (baseIndicatorKey == null || baseIndicatorKey.equals("")) {
            String[] array = key.split("\\.");

            if (array.length > 2) {
                baseIndicatorKey = array[0] + "." + array[1] + "." + array[2];
            } else if (array.length > 1) {
                baseIndicatorKey = array[0] + "." + array[1];
            } else if (array.length > 0) {
                baseIndicatorKey = array[0];
            } else {
                baseIndicatorKey = "";
            }
        }
        return baseIndicatorKey;
    }
}
