package cz.david.mapofdata.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import cz.david.mapofdata.helpers.FileHelper;
import cz.david.mapofdata.helpers.JsonHelper;
import lombok.Data;

import java.util.List;

@Data
public class Topic {

    @JsonProperty
    private int id;
    @JsonProperty
    private String value;
    @JsonIgnore
    private String sourceNote;
    @JsonProperty
    private String imgClass;
    @JsonProperty
    private String color;

    @JsonProperty
    private List<BaseIndicator> baseIndicators;

    public static List<Topic> getListOfTopicsFromFile() throws Exception {
        return new JsonHelper().getTopicsFromJSON(FileHelper.readTopicFile());
    }

    /*private void fillIndicatorsFromFile() throws Exception {
        indicators = new JsonHelper().getIndicatorsFromJSON(FileHelper.readIndicatorFile(getKey()));
    }*/

    /**Spojí dva TOPIC seznamy
     * oldTopics - ponechá imgClass a color**/
    public static List<Topic> uniteTopics(List<Topic> oldTopics, List<Topic> newTopics){
        for (Topic newT : newTopics) {
            for (int o = 0; o < oldTopics.size(); o++) {
                Topic oldT = oldTopics.get(o);
                if (oldT.getId() == newT.getId()) {
                    newT.setColor(oldT.getColor());
                    newT.setImgClass(oldT.getImgClass());
                    oldTopics.remove(oldT);
                    o = oldTopics.size();
                }
            }
        }
        return newTopics;
    }

}
