package cz.david.mapofdata.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.david.mapofdata.helpers.FileHelper;
import cz.david.mapofdata.helpers.JsonHelper;
import lombok.Data;
import org.javatuples.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Data
public class BaseIndicator {

    @JsonProperty
    private String key;
    @JsonProperty
    private String name;

    @JsonProperty
    private List<Indicator> indicators;

    public String getKeyWithoutDots(){
        return key.replace(".", "");
    }

    public void findNameFromIndicators(){
        if (indicators.size() > 1) {

            HashMap<String, String> specialBI = null;

            try {
                JsonHelper jh = new JsonHelper();
                specialBI = jh.getHashMapFromJSON(FileHelper.readSpecialBaseIndicatorsFile());
            }
            catch (IOException ex){
                ex.printStackTrace();
            }

            //prozkoumáme specialBaseIndicators
            if(specialBI != null && specialBI.size() > 0) {
                name = specialBI.get(key);
            }

            //neexistuje name -> vygeneruje se z indikátorů
            if(name == null || name.equals("")) {

                List<String> nameList = new ArrayList<>();
                StringBuilder sName = new StringBuilder();

                List<String> origFirstNameList = Arrays.asList(removeSpecialChars(removeBracket(indicators.get(0).getName())).split(" "));
                List<String> firstNameList = Arrays.asList(removeSpecialChars(removeBracket(indicators.get(0).getName())).toLowerCase().split(" "));

                //vytvořit seznam párů - index, celý název
                List<Pair<Integer, List<String>>> data = new ArrayList<>();

                for (int i = 1; i < indicators.size(); i++) {
                    data.add(new Pair<>(-1,
                            new ArrayList<>(Arrays.asList(removeSpecialChars(removeBracket(indicators.get(i).getName())).toLowerCase().split(" ")))));
                }

                //každé slovo
                for (int wi = 0; wi < firstNameList.size(); wi++) {

                    String word = firstNameList.get(wi);
                    //indikuje zda bylo slovo nalezeno
                    boolean wordFind = false;

                    for (int i = 0; i < data.size(); i++) {

                        Pair<Integer, List<String>> pair = data.get(i);

                        int wordIndex = pair.getValue1().indexOf(word);

                        if (wordIndex > -1) {

                            if (pair.getValue0() == -1 || wordIndex != pair.getValue0() + nameList.size()) {
                                data.set(i, pair.setAt0(wordIndex));
                            }
                            wordFind = true;
                        } else {
                            i = data.size();
                            wordFind = false;
                        }

                    /*if(wi >= pair.second.size()) {
                        wi = firstNameList.size();
                        i = indicators.size();
                        wordFind = false;
                    }
                    else {
                        String nameWord = indicNameList.get(wi);
                        wordFind = nameWord.equals(firstNameList.get(wi));
                    }*/
                    }

                    if (wordFind) {
                        nameList.add(origFirstNameList.get(wi));
                        sName.append(sName.toString().equals("") ? "" : " ").append(origFirstNameList.get(wi));
                    } else {
                        if (nameList.size() > 0) {
                            wi = firstNameList.size();
                        } else {
                            nameList = new ArrayList<>();
                            sName = new StringBuilder();
                        }
                    }
                }

                name = sName.toString();
            }

        } else if (indicators.size() > 0) {
            name = removeBracket(indicators.get(0).getName());
        }
    }

    private String removeLastBracket(String name){
        if(name != null && !name.equals("")) {
            List<String> array = new ArrayList<>(Arrays.asList(name.split("\\(")));
            if (array.size() > 1) {
                array.remove(array.size() - 1);
            }
            return String.join("(", array);
        }
        return "";
    }

    private String removeBracket(String name){
        return name.replaceAll("\\(.*?\\)","");
    }

    private String removeSpecialChars(String name){
        return name.replaceAll("[+.^:,]","");
    }

    /*if (indicators.size() > 1) {

            List<String> nameList = new ArrayList<>();
            StringBuilder sName = new StringBuilder();

            List<String> firstNameList = Arrays.asList(indicators.get(0).getName().split(" "));

            //vytvořit seznam párů - index, celý název
            List<Pair<Integer, List<String>>> data = new ArrayList<>();

            for (int i = 1; i < indicators.size(); i++){
                data.add(new Pair<>(0,
                        new ArrayList<>(Arrays.asList(indicators.get(i).getName().split(" "))) ));
            }

            //každé slovo
            for (int wi = 0; wi < firstNameList.size(); wi++) {

                //indikuje zda jsou všechny slova stejná
                boolean sameWord = false;

                for (int i = 1; i < indicators.size(); i++) {

                    String indicName = indicators.get(i).getName();
                    List<String> indicNameList = Arrays.asList(indicName.split(" "));

                    if(wi >= indicNameList.size()) {
                        wi = firstNameList.size();
                        i = indicators.size();
                        sameWord = false;
                    }
                    else {
                        String nameWord = indicNameList.get(wi);
                        sameWord = nameWord.equals(firstNameList.get(wi));
                    }
                }

                if (sameWord) {
                    nameList.add(firstNameList.get(wi));
                    sName.append(sName.toString().equals("") ? "" : " ").append(firstNameList.get(wi));
                } else {
                    if(nameList.size() > 0){
                        wi = firstNameList.size();
                    }
                    else {
                        nameList = new ArrayList<>();
                        sName = new StringBuilder();
                    }
                }
            }

            name = sName.toString();

        } else if (indicators.size() > 0) {
            name = removeLastBracket(indicators.get(0).getName());
        }*/

}
