package cz.david.mapofdata.helpers;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class RestHelper {

    private Client client;
    private WebTarget target;

    public RestHelper() {
        client = ClientBuilder.newClient();
    }

    public String getTopicsJson() {
        target = client.target(
                "http://api.worldbank.org/v2/topic")
                .queryParam("format", "json")
                .queryParam("per_page", "99"); //100 vrací chybu!

        return target.request(MediaType.APPLICATION_JSON)
                .get(String.class);
        //.get(new GenericType<List<Topic>>(){});
    }

    public String getIndicatorsJson(int topicId) {
        if (topicId < 1) return "";

        target = client.target("http://api.worldbank.org/v2/topic/" + topicId + "/indicator")  //total - 6763... source2 - 2021
                .queryParam("format", "json")
                .queryParam("source", "2")
                .queryParam("per_page", "20000");

        return target.request(MediaType.APPLICATION_JSON)
                .get(String.class);
    }

}
