package cz.david.mapofdata.helpers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileHelper {

    public static String readFile(String fileName) {
        String content = null;
        try {
            content = new String(Files.readAllBytes(Paths.get("data/" + fileName)));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return content;
    }

    public static String readTopicFile() {
        return readFile("topics.json");
    }

    public static String readIndicatorFile(int id) {
        return readFile("indicator_" + id + ".json");
    }

    public static String readSpecialBaseIndicatorsFile() {
        return readFile("specialBI.json");
    }

    public static void writeFile(String content, String fileName) throws IOException {
        Path path = Paths.get("data/" + fileName);
        byte[] strToBytes = content.getBytes();

        Files.write(path, strToBytes);
    }

    public static void writeTopicFile(String content) throws IOException {
        writeFile(content, "topics.json");
    }
}
