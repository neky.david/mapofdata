package cz.david.mapofdata.helpers;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import cz.david.mapofdata.entities.Indicator;
import cz.david.mapofdata.entities.Topic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonHelper {

    private static ObjectMapper mapper;

    public JsonHelper(){
        init();
    }

    private void init(){
        if(mapper == null) {
            mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }
    }

    public String serializObject(Object object) throws JsonProcessingException {
        init();
        return mapper.writeValueAsString(object);
    }

    public Map<String, String> getMapFromJsonFile(String name) {

        String data = FileHelper.readFile(name);
        Map<String, String> map = getMapFromJSON(data);

        return map;
    }

    public Map<String, String> getMapFromJSON(String jsonData) {
        init();
        Map<String, String> map = null;

        try {
            // convert JSON string to Map
            map = mapper.readValue(jsonData, new TypeReference<Map<String, String>>(){});

        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }

    //JsonHelper.getListFromJSON(new Topic(), jsonString, Topic.class);
    public <T> List<T> getListFromJSON(T typeDef,String json,Class clazz) throws IOException
    {
        init();
        List<T> list;

        TypeFactory t = TypeFactory.defaultInstance();
        list = mapper.readValue(json, t.constructCollectionType(ArrayList.class,clazz));

        return list;
    }

    public HashMap<String, String> getHashMapFromJSON(String jsonData) throws IOException{
        init();

        TypeReference<HashMap<String, String>> typeRef
                = new TypeReference<HashMap<String, String>>() {};
        HashMap<String, String> map = mapper.readValue(jsonData, typeRef);

        return map;
    }

    public String getDataPart(String stringJson) throws IOException{
        init();
        return getDataPartNode(stringJson).toString();
    }

    public JsonNode getDataPartNode(String stringJson) throws IOException{
        init();

        JsonNode json = mapper.readTree(stringJson);

        if(json.size() == 2 && json.get(0).get("page") != null){
            json = json.get(1);
        }

        return json;
    }

    public int getCountOfData(String stringJson, boolean findDataPart) throws IOException{
        init();
        JsonNode json;
        if(findDataPart){
            json = getDataPartNode(stringJson);
        }
        else {
            json = mapper.readTree(stringJson);
        }
        return json.size();
    }


    /*UTILS*/
    public List<Topic> getTopicsFromJSON(String jsonData) throws IOException {
        return getListFromJSON(new Topic(), jsonData, Topic.class);
    }

    public List<Indicator> getIndicatorsFromJSON(String jsonData) throws IOException {
        return getListFromJSON(new Indicator(), jsonData, Indicator.class);
    }

}
